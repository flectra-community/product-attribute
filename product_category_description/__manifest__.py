# Copyright 2022 ForgeFlow
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Category Description",
    "summary": "Allows to add a detailed description for a product category.",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "maintainers": ["MarcBForgeFlow"],
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": ["product"],
    "data": ["views/product_category.xml"],
}
