# Copyright (C) 2023 Cetmix OÜ
# Copyright 2020 ForgeFlow S.L.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Lot Sequence",
    "summary": """
        Adds ability to define a lot sequence from the product""",
    "version": "2.0.1.1.0",
    "license": "AGPL-3",
    "author": "ForgeFlow S.L., Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": ["stock"],
    "data": ["views/product_views.xml", "views/res_config_settings.xml"],
}
