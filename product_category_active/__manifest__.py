# Copyright 2020 Tecnativa - Ernesto Tejeda
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

{
    "name": "Product Category Active",
    "version": "2.0.1.0.0",
    "category": "Product",
    "summary": "Add option to archive product categories",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "license": "AGPL-3",
    "depends": ["product"],
    "data": ["views/product_views.xml"],
}
