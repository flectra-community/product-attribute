# Copyright 2021 Akretion
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Attribute Archive",
    "summary": """
        Add an active field on product attributes""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Akretion,Odoo Community Association (OCA)",
    "depends": ["product"],
    "data": [
        "views/product_attribute.xml",
    ],
    "website": "https://gitlab.com/flectra-community/product-attribute",
}
