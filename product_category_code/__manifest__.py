# Copyright 2020 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Category Code",
    "summary": """
        Allows to define a code on product categories""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "maintainers": ["rousseldenis"],
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": [
        "product",
    ],
    "data": [
        "views/product_category.xml",
    ],
}
