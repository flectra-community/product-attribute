# Copyright 2021 Camptocamp SA
# @author Iván Todorovich <ivan.todorovich@gmail.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Sale Product Template Tags",
    "summary": "Show product tags menu in Sale app",
    "version": "2.0.1.1.0",
    "license": "AGPL-3",
    "author": "Camptocamp SA, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": ["product_template_tags", "sale"],
    "data": ["views/product_template_tag.xml"],
    "maintainers": ["ivantodorovich"],
    "auto_install": True,
}
