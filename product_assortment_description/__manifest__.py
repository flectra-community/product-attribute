{
    "name": "Product Assortment Description",
    "summary": """
        Description field for assortment""",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "development_status": "Production/Stable",
    "author": "Ilyas, Ooops404, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": ["product_assortment"],
    "data": ["views/product_assortment.xml"],
    "installable": True,
}
