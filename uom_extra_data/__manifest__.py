# Copyright 2021 Camptocamp SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl)
{
    "name": "uom_extra_data",
    "summary": "Have mm, ml, week, work_week uom",
    "version": "2.0.1.0.1",
    "category": "Product",
    "author": "Camptocamp, Trobz, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": [
        "uom",
    ],
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "data": [
        "data/uom.xml",
    ],
    "installable": True,
}
