# Copyright 2020 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Pricelist Button Box",
    "summary": """
        Allows to define a button_box section on pricelist form""",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "maintainers": ["rousseldenis"],
    "depends": [
        "product",
    ],
    "data": [
        "views/product_pricelist.xml",
    ],
}
