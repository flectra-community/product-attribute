# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Product Variant Company",
    "summary": "Product Company Storage",
    "version": "2.0.1.0.0",
    "category": "Product",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "author": "Ooops, Cetmix, Odoo Community Association (OCA)",
    "maintainers": ["CetmixGitDrone", "Volodiay622"],
    "license": "AGPL-3",
    "installable": True,
    "depends": [
        "product",
    ],
    "data": [
        "security/product_security.xml",
    ],
}
