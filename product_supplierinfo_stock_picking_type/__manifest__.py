# Copyright 2022 Tecnativa - Víctor Martínez
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Product supplierinfo stock picking type",
    "version": "2.0.1.0.0",
    "category": "Product",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["purchase_stock"],
    "installable": True,
    "data": [
        "views/product_supplierinfo_view.xml",
    ],
    "maintainers": ["victoralmau"],
}
