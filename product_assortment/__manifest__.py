# Copyright 2021 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Assortment",
    "summary": """
        Adds the ability to manage products assortment""",
    "version": "2.0.1.1.1",
    "license": "AGPL-3",
    "development_status": "Production/Stable",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": ["base", "product"],
    "data": ["views/product_assortment.xml", "views/res_partner_view.xml"],
    "installable": True,
}
