# Flectra Community / product-attribute

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[product_category_description](product_category_description/) | 2.0.1.0.0| Allows to add a detailed description for a product category.
[product_pricelist_assortment](product_pricelist_assortment/) | 2.0.2.2.0|         Product assortment and pricelist
[product_product_template_navigation](product_product_template_navigation/) | 2.0.1.0.1| Adds a button in product to view the template
[product_packaging_unit_price_calculator](product_packaging_unit_price_calculator/) | 2.0.1.1.1| Wizard to calculate a unit price from a packaging price
[product_bom_revision](product_bom_revision/) | 2.0.1.0.1| Store the revision of a product and a BOM
[product_category_product_link](product_category_product_link/) | 2.0.1.0.1|         Allows to get products from a category
[sale_product_template_tags](sale_product_template_tags/) | 2.0.1.1.0| Show product tags menu in Sale app
[product_dimension](product_dimension/) | 2.0.1.0.0| Product Dimension
[product_attribute_archive](product_attribute_archive/) | 2.0.1.0.0|         Add an active field on product attributes
[product_lot_sequence](product_lot_sequence/) | 2.0.1.1.0|         Adds ability to define a lot sequence from the product
[product_supplierinfo_archive](product_supplierinfo_archive/) | 2.0.1.1.0|         Add the active field to the product supplier info        
[product_tier_validation](product_tier_validation/) | 2.0.2.0.2| Support a tier validation process for Products
[product_category_code_unique](product_category_code_unique/) | 2.0.1.0.0|         Allows to set product category code field as unique
[product_total_weight_from_packaging](product_total_weight_from_packaging/) | 2.0.1.1.1| Compute estimated weight based on product's packaging weights
[product_supplierinfo_group](product_supplierinfo_group/) | 2.0.2.0.3| Product Supplierinfo Group
[product_multi_image](product_multi_image/) | 2.0.1.0.1| Multiple Images in Products
[product_order_noname](product_order_noname/) | 2.0.2.0.0| Speedup product retrieve
[product_net_weight](product_net_weight/) | 2.0.1.0.1| Add 'Net Weight' on product models
[pos_product_cost_security](pos_product_cost_security/) | 2.0.1.0.2| Compatibility between Point of Sale and Product Cost Security
[product_assortment_description](product_assortment_description/) | 2.0.1.0.1|         Description field for assortment
[product_pricelist_direct_print_company_group](product_pricelist_direct_print_company_group/) | 2.0.1.0.0| Print Pricelist items using the company group model
[product_assortment](product_assortment/) | 2.0.1.1.1|         Adds the ability to manage products assortment
[product_categ_image](product_categ_image/) | 2.0.1.0.0| Add image on product category
[product_main_supplierinfo](product_main_supplierinfo/) | 2.0.1.0.2| Display the main vendor of a product.
[product_supplierinfo_for_customer](product_supplierinfo_for_customer/) | 2.0.2.0.3| Allows to define prices for customers in the products
[product_code_unique](product_code_unique/) | 2.0.1.0.0| Set Product Internal Reference as Unique
[product_uom_updatable](product_uom_updatable/) | 2.0.1.0.1|         allows products uom to be modified after be used in a stock picking if          the product uom is of the same category
[stock_product_template_tags](stock_product_template_tags/) | 2.0.1.1.0| Show product tags menu in Inventory app
[product_medical](product_medical/) | 2.0.1.0.1| Base structure to handle medical products
[product_pricelist_supplierinfo](product_pricelist_supplierinfo/) | 2.0.1.1.1| Allows to create priceslists based on supplier info
[base_product_mass_addition](base_product_mass_addition/) | 2.0.1.2.0| Base Product Mass Addition
[packaging_uom](packaging_uom/) | 2.0.1.0.1| Use uom in package
[product_sale_manufactured_for](product_sale_manufactured_for/) | 2.0.1.1.0| Allows to indicate in products that they were made specifically for some customers.
[product_manufacturer](product_manufacturer/) | 2.0.1.0.2| Adds manufacturers and attributes on the product view.
[product_internal_reference_generator](product_internal_reference_generator/) | 2.0.1.1.1| Product template and variant reference based on sequence
[product_pricelist_button_box](product_pricelist_button_box/) | 2.0.1.0.1|         Allows to define a button_box section on pricelist form
[product_packaging_type_vendor](product_packaging_type_vendor/) | 2.0.1.0.0| Allows to mark a packaging type as vendor specific
[product_supplierinfo_revision](product_supplierinfo_revision/) | 2.0.1.0.0| Product Supplierinfo Revision
[product_category_active](product_category_active/) | 2.0.1.0.0| Add option to archive product categories
[uom_extra_data](uom_extra_data/) | 2.0.1.0.1| Have mm, ml, week, work_week uom
[product_packaging_type_pallet](product_packaging_type_pallet/) | 2.0.1.0.1| Manage packaging of pallet type
[product_weight_logistics_uom](product_weight_logistics_uom/) | 2.0.1.0.0| Glue module for product_weight and product_logistics_uom
[product_weight](product_weight/) | 2.0.1.0.1| Allows to calculate products weight from its components.
[product_category_code](product_category_code/) | 2.0.1.0.0|         Allows to define a code on product categories
[product_logistics_uom](product_logistics_uom/) | 2.0.1.0.2| Configure product weights and volume UoM
[product_seasonality](product_seasonality/) | 2.0.1.3.0| Define rules for products' seasonal availability
[product_state_history](product_state_history/) | 2.0.1.0.2|         Allows to store product state history for reporting purpose
[product_search_by_display_name](product_search_by_display_name/) | 2.0.1.0.0| Product Search By Display Name
[product_state_active](product_state_active/) | 2.0.1.0.0|         Allows to define option on product state to activate or inactivate product
[product_model_viewer](product_model_viewer/) | 2.0.1.0.1| 3D model viewer for products
[product_pricelist_direct_print](product_pricelist_direct_print/) | 2.0.2.0.0| Print price list from menu option, product templates, products variants or price lists
[product_code_mandatory](product_code_mandatory/) | 2.0.1.0.1| Set Product Internal Reference as a required field
[product_video_link](product_video_link/) | 2.0.1.0.2| Link Video on product and category
[product_category_type](product_category_type/) | 2.0.1.0.1|         Add Type field on Product Categories        to distinguish between parent and final categories
[product_state](product_state/) | 2.0.1.1.5|         Module introducing a state field on product template
[purchase_product_template_tags](purchase_product_template_tags/) | 2.0.1.1.0| Show product tags menu in Purchase app
[product_cost_security](product_cost_security/) | 2.0.1.0.0| Product cost security restriction view
[product_variant_company](product_variant_company/) | 2.0.1.0.0| Product Company Storage
[product_restricted_type](product_restricted_type/) | 2.0.1.0.1| Product Restricted Type
[product_secondary_unit](product_secondary_unit/) | 2.0.1.0.4| Set a secondary unit per product
[product_barcode_required](product_barcode_required/) | 2.0.1.0.0| Make product barcode required when enabled
[product_template_tags](product_template_tags/) | 2.0.1.2.0| This addon allow to add tags on products
[product_template_tags_code](product_template_tags_code/) | 2.0.1.0.1| This addon allow to add code on products tags
[product_custom_info](product_custom_info/) | 2.0.1.0.1| Add custom field in products
[product_supplierinfo_stock_picking_type](product_supplierinfo_stock_picking_type/) | 2.0.1.0.0| Product supplierinfo stock picking type
[product_multi_category](product_multi_category/) | 2.0.1.0.0| Product - Many Categories
[product_attribute_value_menu](product_attribute_value_menu/) | 2.0.2.0.2| Product attributes values tree and form. Import attribute values.
[product_stock_state](product_stock_state/) | 2.0.1.0.1| Compute the state of a product's stockthe stock level and sale_ok field
[product_sequence](product_sequence/) | 2.0.2.0.1| Product Sequence
[product_packaging_dimension](product_packaging_dimension/) | 2.0.1.0.2| Manage packaging dimensions and weight
[product_variant_attribute_name_manager](product_variant_attribute_name_manager/) | 2.0.1.1.1| Manage how to display the attributes on the product variant name.
[product_profile](product_profile/) | 2.0.1.0.1| Allow to configure a product in 1 click
[product_supplierinfo_for_customer_group](product_supplierinfo_for_customer_group/) | 2.0.1.0.0| Fixes compatibility of product_supplierinfo_for_customer and product_supplierinfo_group
[product_status](product_status/) | 2.0.1.0.3| Product Status Computed From Fields
[product_packaging_type](product_packaging_type/) | 2.0.0.2.0| Product Packaging Type
[product_expiry_configurable](product_expiry_configurable/) | 2.0.1.0.0|         This model allows setting expiry times on category and         to use the 'end_of_life' date for the computation of lot dates
[product_pricelist_revision](product_pricelist_revision/) | 2.0.1.0.2| Product Pricelist Revision
[product_packaging_type_required](product_packaging_type_required/) | 2.0.1.1.0| Product Packaging Type Required
[product_form_pricelist](product_form_pricelist/) | 2.0.1.0.1| Show/edit pricelist in product form


