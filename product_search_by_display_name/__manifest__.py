# Copyright 2023 Komit - Cuong Nguyen Mtm
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Product Search By Display Name",
    "version": "2.0.1.0.0",
    "installable": True,
    "category": "Product",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "author": "KOMIT, Odoo Community Association (OCA)",
    "maintainers": ["cuongnmtm"],
    "license": "AGPL-3",
    "depends": ["product"],
    "data": [
        "views/product_template.xml",
    ],
    "application": False,
}
