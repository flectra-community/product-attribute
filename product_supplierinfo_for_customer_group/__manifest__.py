{
    "name": "Product Supplierinfo for Customers and Product "
    "Supplierinfo Group compatibility",
    "summary": "Fixes compatibility of product_supplierinfo_for_customer "
    "and product_supplierinfo_group",
    "version": "2.0.1.0.0",
    "author": "Ilyas, Ooops404, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "category": "Sales Management",
    "license": "AGPL-3",
    "depends": ["product_supplierinfo_for_customer", "product_supplierinfo_group"],
    "installable": True,
    "auto_install": True,
}
