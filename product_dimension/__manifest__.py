# Copyright 2018 brain-tec AG (http://www.braintec-group.com)
# Copyright 2015-2016 Camptocamp SA
# Copyright 2015 ADHOC SA  (http://www.adhoc.com.ar)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    "name": "Product Dimension",
    "version": "2.0.1.0.0",
    "category": "Product",
    "author": "brain-tec AG, ADHOC SA, Camptocamp SA, "
    "Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": ["product"],
    "data": ["views/product_view.xml"],
    "installable": True,
    "images": ["static/description/icon.png"],
}
