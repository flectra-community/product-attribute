# Copyright 2019 Alexandre Díaz - Tecnativa
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Product Template Navigation",
    "summary": "Adds a button in product to view the template",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "category": "Product",
    "version": "2.0.1.0.1",
    "license": "AGPL-3",
    "depends": ["product"],
    "data": ["views/product_product_views.xml"],
    "installable": True,
}
