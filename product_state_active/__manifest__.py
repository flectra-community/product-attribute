# Copyright 2021 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product State Active",
    "summary": """
        Allows to define option on product state to activate or inactivate product""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "maintainers": ["rousseldenis"],
    "website": "https://gitlab.com/flectra-community/product-attribute",
    "depends": [
        "product_state",
    ],
    "data": [
        "views/product_state.xml",
    ],
}
